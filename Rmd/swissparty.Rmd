---
editor_options:
  chunk_output_type: console
---

# INTERNAL

## Avoid `R CMD check` notes about undefined global objects used in magrittr pipes

cf. <https://github.com/tidyverse/magrittr/issues/29#issuecomment-74313262>

```{r}
utils::globalVariables(names = c("."))
```

## Constants

TODO: finish structured party data; base on SRF Data's [swiss-party-colors](https://github.com/srfdata/swiss-party-colors) (underlying data is found in [this
Google Sheet](https://docs.google.com/spreadsheets/d/1PCD3se4Nc4ME-i391yPYyAlLdgtXoZJFoJy_6Mlf7BY/edit#gid=0))

### `regex_party`

WIP

It is adhered to the parties' own official spelling as far as possible.

```{r, purl = FALSE}
regex_party <-
  tibble::tribble(
    
    ~party, ~regex, ~url,
    
    "Jungsozialist*innen Schweiz (JUSO)", "J(USO|uso)", "https://juso.ch/",
    "Sozialdemokratische Partei der Schweiz (SP)", "(SP|Sozialdemokratische Partei)", "https://www.sp-ps.ch/",
    "Junge Gr\u00fcne Schweiz", "Junge G(r\u00fcne|R\u00dcNE)",
    "GR\u00dcNE Schweiz", "Gr\u00fcne",
    "Junge Evangelische Volkspartei (*jevp)", "(Junge EVP|\\*?(jevp|JEVP))", "https://www.jevp.ch/",
    "EVP",                                     # EVP
    "LOVB",                                    # LOVB
    "SLB",                                     # SLB
    "Junge (GLP|glp)",                         # Junge glp
    "(GLP|glp)",                               # glp
    "((Piraten)partei?|PPS)",                  # Piraten
    "IP",                                      # IP (Integrale Politik)
    "Pius Lischer",                            # Pius Lischer (Neue Bundesverfassung / Fuer Freiheit und Gesundheit)
    "Stephan Zurfluh",                         # Stephan Zurfluh (i54.ch)
    "Frecher Frischer Fischer",                # Frecher Frischer Fischer
    paste0("\\Q", lvl_below_min, "\\E")[lump], # _lumped together small parties_
    "mehrere \\(links",                        # multiple (left)
    "mehrere \\(mitte-rechts",                 # multiple (center-right)
    "mehrere \\(beliebig",                     # multiple (arbitrary)
    "(andere[rs]?|other|custom)",              # _custom answers_
    "(DU|Die Unabh\u00e4ngigen)",              # DU (Die Unabhaengigen)
    "(FW|Freie W\u00e4hler)",                  # Freie Waehler
    "(?i)nichtw\u00e4hler",                    # www.Nichtwaehler.ch
    "Junge BDP",                               # Junge BDP
    "BDP",                                     # BDP
    "Junge CVP",                               # Junge CVP
    "CVP",                                     # CVP
    "Jungfreisinnige",                         # Jungfreisinnige
    "FDP",                                     # FDP
    "Ecopop",                                  # Ecopop
    "Luzi Stamm",                              # Luzi Stamm
    "TEAM65+",                                 # TEAM65+
    "Junge SVP",                               # Junge SVP
    "SVP",                                     # SVP
    "Junge EDU",                               # Junge EDU
    "EDU",                                    # EDU
  ) %>%
  # add word boundaries
  dplyr::mutate(regex = paste0("\\b", regex, "\\b"))
```

# EXPORTED

## Factor-specific

### `reorder_party_fct`

TODO:

-   Do not hardcode `empty_party_regex` but source it from central fn instead.

-   Overthink `min_share`! `forcats::fct_lump_min(w = )` allows to provide a weight variable, so we should *really* use that! Also it would be cool to be able
    to provide either `min_share` or `min_n` with the latter allowing to specifiy the minimum number of (weighted) cases.

-   Make `lvl_below_min` dependent on language.

```{r}
#' Reorder a party-referencing factor according to the usual left-right spectrum
#'
#' Reorders a factor whose levels reference Swiss political parties according to the usual left-right spectrum. Levels that make up less than `min_share` of all
#' values will be lumped together to the new factor level `lvl_below_min` which will be placed in the center of the left-right spectrum.
#'
#' @param fct The factor to be reordered. Its levels should reference Swiss political parties.
#' @param min_share The share of `fct`'s length below which levels are lumped together to `lvl_below_min`. Set to `0` for no lumping.
#' @param lvl_below_min The name of the new factor level for all the old levels that fell below `min_share`.
#' @param abstention_lvl_right Whether to place `fct` levels for vote abstention at the right pole of the left-right spectrum. If `FALSE`, abstention levels
#'   will be placed at the left pole instead.
#'
#' @return A factor.
#' @family fct
#' @export
reorder_party_fct <- function(fct,
                              min_share = 0.02,
                              lvl_below_min = "Kleinparteien",
                              abstention_lvl_right = TRUE) {
  
  fct <- checkmate::assert_factor(fct)
  min_share <- checkmate::assert_number(min_share,
                                        lower = 0.0,
                                        upper = 1.0)
  lvl_below_min <- checkmate::assert_string(lvl_below_min)
  abstention_lvl_right <- checkmate::assert_flag(abstention_lvl_right)
  lump <- min_share > 0L
  
  # empty votes regex
  empty_party_regex <- paste0("^(keine \\((leer eingelegt|nicht teilgenommen)( oder (nicht teilgenommen|leer eingelegt))?\\)",
                              "|empty \\(blank vote\\) or not voted",
                              "|keine",
                              "|none)$")
  if (lump) {
    
    # ensure "empty votes" category is *never* lumped together to `lvl_below_min`
    empty_share <-
      fct %>%
      stringr::str_detect(pattern = empty_party_regex) %>%
      which() %>%
      length() %>%
      magrittr::divide_by(length(fct))
    
    if (empty_share > 0L && empty_share < min_share) {
      cli::cli_abort(paste0("Empty votes category included in {.arg fct} which makes up {.val {pal::round_to(empty_share, to = 0.01)}} of all values. This is ",
                            "less than {.code min_share = {min_share}} and thus would be lumped together to {.code lvl_below_min = \"{lvl_below_min}\"}."))
    }
    
    fct %<>% forcats::fct_lump_min(min = length(.) * min_share,
                                   other_level = lvl_below_min)
  }
  
  # party order from left to right
  regex_party_order <- paste0("\\b", 
                              c("JUSO",                                    # JUSO
                                "SP",                                      # SP
                                "Junge Gr\u00fcne",                        # Junge Gruene
                                "Gr\u00fcne",                              # Gruene
                                "Junge EVP",                               # Junge EVP
                                "EVP",                                     # EVP
                                "LOVB",                                    # LOVB
                                "SLB",                                     # SLB
                                "Junge (GLP|glp)",                         # Junge GLP
                                "(GLP|glp)",                               # GLP
                                "((Piraten)partei?|PPS)",                  # Piraten
                                "IP",                                      # IP (Integrale Politik)
                                "Pius Lischer",                            # Pius Lischer ("Neue Bundesverfassung", "Fuer Freiheit und Gesundheit", etc.)
                                "Stephan Zurfluh",                         # Stephan Zurfluh ("i54.ch", "Musikpartei")
                                "Frecher Frischer Fischer",                # Frecher Frischer Fischer
                                paste0("\\Q", lvl_below_min, "\\E")[lump], # _lumped together small parties_
                                "mehrere \\(links",                        # multiple (left)
                                "mehrere \\(mitte-rechts",                 # multiple (center-right)
                                "mehrere \\(beliebig",                     # multiple (arbitrary)
                                "(andere[rs]?|other|custom)",              # _custom answers_
                                "(DU|Die Unabh\u00e4ngigen)",              # DU (Die Unabhaengigen)
                                "(FW|Freie W\u00e4hler)",                  # Freie Waehler
                                "(?i)nichtw\u00e4hler",                    # www.Nichtwaehler.ch
                                "Junge BDP",                               # Junge BDP
                                "BDP",                                     # BDP
                                "(Die )?Mitte",                            # Die Mitte
                                "(Die )?[Jj]unge Mitte",                   # Die Junge Mitte
                                "Junge CVP",                               # Junge CVP
                                "CVP",                                     # CVP
                                "Jungfreisinnige",                         # Jungfreisinnige
                                "FDP",                                     # FDP
                                "Ecopop",                                  # Ecopop
                                "Luzi Stamm",                              # Luzi Stamm
                                "TEAM65+",                                 # TEAM65+
                                "Junge SVP",                               # Junge SVP
                                "SVP",                                     # SVP
                                "(DBP|DieB\u00fcrgerPartei)",              # DBP (DieBürgerPartei)
                                "Junge EDU",                               # Junge EDU
                                "EDU"),                                    # EDU
                              "\\b")
  
  if (abstention_lvl_right) {
    regex_party_order %<>% c(empty_party_regex)
  } else {
    regex_party_order %<>% c(empty_party_regex, .)
  }
    
  lvls <- levels(fct)
  
  new_lvls <-
    regex_party_order %>%
    purrr::map(.f = stringr::str_subset,
               string = lvls) %>%
    purrr::compact() %>%
    unique() %>%
    purrr::list_c(ptype = character())
  
  # ensure we've matched all lvls
  unmatched_lvls <- setdiff(lvls,
                            new_lvls)
  
  if (length(unmatched_lvls) > 0L) {
    cli::cli_abort(paste0("Left-right party ordering failed due to unmatched factor {cli::qty(unmatched_lvls)} level{?s} {.val {unmatched_lvls}}. This most ",
                          "likely means the regex rules need to be updated."),
                   .internal = TRUE)
  }
  
  fct %>% forcats::fct_relevel(new_lvls)
}
```
